package com.tuczen.senders.repositories

import com.tuczen.senders.models.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
interface UserRepository: JpaRepository<User, UUID> {

    @Transactional
    fun deleteByIdAndSenderId(id: UUID, senderId: UUID)
}