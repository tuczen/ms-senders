package com.tuczen.senders.repositories

import com.tuczen.senders.models.Sender
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SenderRepository: JpaRepository<Sender, UUID> {
}