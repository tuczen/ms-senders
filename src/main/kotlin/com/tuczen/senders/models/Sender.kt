package com.tuczen.senders.models

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Sender {

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true, columnDefinition = "BINARY(16)")
    var id: UUID? = null

    @Column(nullable = false)
    var name: String? = null
}