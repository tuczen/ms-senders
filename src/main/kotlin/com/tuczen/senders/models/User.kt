package com.tuczen.senders.models

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class User {

    @Id
    @Column(nullable = false, columnDefinition = "BINARY(16)")
    var id: UUID? = null

    @Column(nullable = false, columnDefinition = "BINARY(16)")
    var senderId: UUID? = null
}