package com.tuczen.senders.services

import com.tuczen.senders.models.Sender
import java.util.*

interface ISenderService {
    fun findAll(): List<Sender>

    fun save(entity: Sender): Sender

    fun find(id: UUID): Sender

    fun update(id: UUID, entity: Sender): Sender

    fun destroy(id: UUID)
}