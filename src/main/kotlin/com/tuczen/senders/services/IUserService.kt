package com.tuczen.senders.services

import com.tuczen.senders.models.User
import java.util.*

interface IUserService {
    fun findAll(): List<User>

    fun save(entity: User): User

    fun destroy(id: UUID, senderId: UUID)
}