package com.tuczen.senders.services

import com.tuczen.senders.exceptions.ServiceException
import com.tuczen.senders.models.User
import com.tuczen.senders.repositories.UserRepository
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.jvm.Throws

@Service
class UserService: IUserService {

    @Autowired
    val driverRepository: UserRepository? = null

    @Throws(ServiceException::class)
    override fun findAll(): List<User> {
        try {
            return driverRepository!!.findAll()
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class)
    override fun save(entity: User): User {
        try {
            return driverRepository!!.save(entity)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class, NotFoundException::class)
    override fun destroy(id: UUID, dealerId: UUID) {
        findOrFail(id)

        try {
            driverRepository!!.deleteByIdAndSenderId(id, dealerId)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    fun findOrFail(id: UUID): User {
        val op: Optional<User>

        try {
            op = driverRepository!!.findById(id)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }

        if (!op.isPresent) {
            throw NotFoundException("Model not found")
        }

        return op.get()
    }
}