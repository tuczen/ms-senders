package com.tuczen.senders.services

import com.tuczen.senders.exceptions.ServiceException
import com.tuczen.senders.models.Sender
import com.tuczen.senders.repositories.SenderRepository
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.jvm.Throws

@Service
class SenderService: ISenderService {

    @Autowired
    val senderRepository: SenderRepository? = null

    @Throws(ServiceException::class)
    override fun findAll(): List<Sender> {
        try {
            return senderRepository!!.findAll()
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class)
    override fun save(entity: Sender): Sender {
        try {
            return senderRepository!!.save(entity)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class, NotFoundException::class)
    override fun find(id: UUID): Sender {
        return findOrFail(id)
    }

    @Throws(ServiceException::class, NotFoundException::class)
    override fun update(id: UUID, entity: Sender): Sender {
        findOrFail(id)

        try {
            return senderRepository!!.save(entity)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    @Throws(ServiceException::class, NotFoundException::class)
    override fun destroy(id: UUID) {
        findOrFail(id)

        try {
            senderRepository!!.deleteById(id);
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }
    }

    fun findOrFail(id: UUID): Sender {
        val op: Optional<Sender>

        try {
            op = senderRepository!!.findById(id)
        } catch (e: Exception) {
            throw ServiceException(e.message)
        }

        if (!op.isPresent) {
            throw NotFoundException("Model not found")
        }

        return op.get()
    }
}