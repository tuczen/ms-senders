package com.tuczen.senders

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SendersApplication

fun main(args: Array<String>) {
	runApplication<SendersApplication>(*args)
}
