package com.tuczen.senders.controllers

import com.tuczen.senders.models.Sender
import com.tuczen.senders.services.ISenderService
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.PathVariable

import org.springframework.web.bind.annotation.GetMapping

import org.springframework.web.bind.annotation.RequestBody

import org.springframework.web.bind.annotation.PutMapping

import org.springframework.web.bind.annotation.DeleteMapping
import java.util.*


@RestController
@RequestMapping("api/v1/senders")
class SendersController {

    @Autowired
    val senderService: ISenderService? = null

    @GetMapping
    fun index(): ResponseEntity<List<Sender>> {
        return ResponseEntity(senderService!!.findAll(), HttpStatus.OK)
    }

    @PostMapping
    fun create(@RequestBody sender: Sender): ResponseEntity<Any> {
        senderService!!.save(sender)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @DeleteMapping("{id}")
    fun destroy(@PathVariable("id") id: UUID): ResponseEntity<Any> {
        return try {
            senderService!!.destroy(id)
            ResponseEntity(HttpStatus.OK)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("{id}")
    fun update(@PathVariable id: UUID, @RequestBody sender: Sender): ResponseEntity<Any> {
        return try {
            senderService!!.update(id, sender)
            ResponseEntity(HttpStatus.OK)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("{id}")
    fun show(@PathVariable("id") id: UUID): ResponseEntity<Any> {
        return try {
            ResponseEntity(senderService!!.find(id), HttpStatus.OK)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}