package com.tuczen.senders.controllers;

import com.tuczen.senders.models.User
import com.tuczen.senders.services.IUserService
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("api/v1/senders/{senderId}/users")
class DealerUsersController {

    @Autowired
    val userService: IUserService? = null

    @GetMapping
    fun index(@PathVariable("senderId") senderId: UUID): ResponseEntity<List<User>> {
        return ResponseEntity(userService!!.findAll(), HttpStatus.OK)
    }

    @PostMapping
    fun create(@PathVariable("senderId") senderId: UUID, @RequestBody user: User): ResponseEntity<Any> {
        user.senderId = senderId
        userService!!.save(user)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @DeleteMapping("{id}")
    fun destroy(@PathVariable("senderId") senderId: UUID, @PathVariable("id") id: UUID): ResponseEntity<Any> {
        return try {
            userService!!.destroy(id, senderId)
            ResponseEntity(HttpStatus.OK)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
